package walletsvc

import (
	"bitbucket.org/kovalevm/walletsvc/account"
	"bitbucket.org/kovalevm/walletsvc/payment"
	"context"
	"time"

	"github.com/go-kit/kit/log"
)

type loggingService struct {
	logger log.Logger
	Service
}

// NewLoggingService returns a new instance of a logging Service.
func NewLoggingService(logger log.Logger, s Service) Service {
	return &loggingService{logger, s}
}

func (s *loggingService) Accounts(lr *listRequest) ([]*account.Account, error) {
	defer func(begin time.Time) {
		s.logger.Log(
			"method", "list_accounts",
			"took", time.Since(begin),
		)
	}(time.Now())
	return s.Service.Accounts(lr)
}

func (s *loggingService) Payments(lr *listRequest) ([]*payment.Payment, error) {
	defer func(begin time.Time) {
		s.logger.Log(
			"method", "list_payments",
			"took", time.Since(begin),
		)
	}(time.Now())
	return s.Service.Payments(lr)
}

func (s *loggingService) SendPayment(ctx context.Context, p *payment.Payment) error {
	defer func(begin time.Time) {
		s.logger.Log(
			"method", "send_payment",
			"took", time.Since(begin),
		)
	}(time.Now())
	return s.Service.SendPayment(ctx, p)
}
