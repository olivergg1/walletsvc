## Send a payment
Transfer money from one account to another.

* **URL:** `/v1/payments`
* **Method:** `POST`
* **URL Params:** None
* **Body JSON Params:**
	* `account` - account ID for taking money (string)
	* `to_account` - account ID for receive money (string)
	* `amount` - amount of money to transfer (float)
	* `direction` - direction of transfer, can be "incoming" and "outgoing". "outgoing" 
	means swap `account` and `to_account` field


* **Success Response:**

  * **Code:** 200 <br />
    **Content:** `{}`
 
* **Error Responses:**

  * **Code:** 400 BAD REQUEST <br />
    * **Content:** `{ "error": "'account' and 'to_account' can't be same" }`
    * **Content:** `{ "error": "'account' and 'to_account' args can't be empty"}`
    * **Content:** `{ "error": "'amount' arg is incorrect: should be positive number" }`
    * _and so on..._
 
  * **Code:** 404 NOT FOUND <br />
    **Content:** `{ "error": "unknown account" }`

* **Sample Call:**

  ```sh
  curl -X POST \
    http://localhost:8081/v1/payments \
    -H 'content-type: application/json' \
    -d '{
  	"account": "tony123",
  	"to_account": "adam321",
  	"amount": 0.5,
  	"direction": "incoming"
  }'
  ```

## List of payments
* **URL:** `/v1/payments`
* **Method:** `GET`
* **URL Params:**
	* `limit` - limit for results. By default - 100
	* `offset` - offset of results. By default - 0

* **Success Response:**

  * **Code:** 200 <br />
    **Content:** <br />
    ```
    {
        "payments": [
            { "account": "adam", "amount": 1, "to_account": "sara", "direction": "outgoing"},
            { "account": "lucy", "amount": 1.5, "to_account": "adam", "direction": "incoming"}
        ]
    }
    ```
 
* **Error Responses:**

  * **Code:** 400 BAD REQUEST <br />
    * **Content:** `{ "error": "invalid 'limit' value, should be positive number" }`
    * _and so on..._
 

* **Sample Call:**

  ```sh
  curl -X GET \
    http://localhost:8081/v1/payments \
    -H 'content-type: application/json'
  ```


**List of accounts**
----
* **URL:** `/v1/accounts`
* **Method:** `GET`
* **URL Params:**
	* `limit` - limit for results. By default - 100
	* `offset` - offset of results. By default - 0

* **Success Response:**

  * **Code:** 200 <br />
    **Content:** <br />
    ```
    { 
        "accounts": [ 
            {"id": "sara", "balance": 5.1, "currency": "USD"}, 
            {"id": "tony", "balance": 2, "currency": "USD"}
        ] 
    }
    ```
 
* **Error Responses:**

  * **Code:** 400 BAD REQUEST <br />
    * **Content:** `{ "error": "invalid 'limit' value, should be positive number" }`
    * _and so on..._
 

* **Sample Call:**

  ```sh
  curl -X GET \
    http://localhost:8081/v1/accounts \
    -H 'content-type: application/json'
  ```
