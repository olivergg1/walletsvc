package walletsvc

import (
	"bitbucket.org/kovalevm/walletsvc/payment"
	"context"
	"encoding/json"
	"fmt"
	kitlog "github.com/go-kit/kit/log"
	"github.com/go-kit/kit/transport"
	kithttp "github.com/go-kit/kit/transport/http"
	"github.com/gorilla/mux"
	"net/http"
	"strconv"
)

// MakeHandler returns a handler for the wallet service.
func MakeHandler(bs Service, logger kitlog.Logger) http.Handler {
	opts := []kithttp.ServerOption{
		kithttp.ServerErrorHandler(transport.NewLogErrorHandler(logger)),
		kithttp.ServerErrorEncoder(encodeError),
	}

	listAccountsHandler := kithttp.NewServer(
		makeListAccountsEndpoint(bs),
		decodeListRequest,
		encodeResponse,
		opts...,
	)

	listPaymentsHandler := kithttp.NewServer(
		makeListPaymentsEndpoint(bs),
		decodeListRequest,
		encodeResponse,
		opts...,
	)

	sendPaymentHandler := kithttp.NewServer(
		makeSendPaymentEndpoint(bs),
		decodeSendPaymentRequest,
		encodeResponse,
		opts...,
	)

	r := mux.NewRouter()

	r.Handle("/v1/accounts", listAccountsHandler).Methods("GET")
	r.Handle("/v1/payments", listPaymentsHandler).Methods("GET")
	r.Handle("/v1/payments", sendPaymentHandler).Methods("POST")

	return r
}

func decodeListRequest(_ context.Context, r *http.Request) (interface{}, error) {
	var (
		limit  = 100 // by default
		offset = 0   // by default
		err    error
	)

	keys := r.URL.Query()

	if str := keys.Get("limit"); str != "" {
		limit, err = strconv.Atoi(str)
		if err != nil {
			return nil, NewErrInvalidArg(fmt.Errorf("invalid 'limit' value, should be integer, given: '%s'", str))
		}
	}

	if str := keys.Get("offset"); str != "" {
		offset, err = strconv.Atoi(str)
		if err != nil {
			return nil, NewErrInvalidArg(fmt.Errorf("invalid 'offset' value, should be integer, given: '%s'", str))
		}
	}

	return listRequest{Limit: limit, Offset: offset}, nil
}

func decodeSendPaymentRequest(_ context.Context, r *http.Request) (interface{}, error) {
	var p payment.Payment

	if err := json.NewDecoder(r.Body).Decode(&p); err != nil {
		return nil, NewErrInvalidArg(fmt.Errorf("failed to decode request: %s", err))
	}

	return &p, nil
}

func encodeResponse(ctx context.Context, w http.ResponseWriter, response interface{}) error {
	if e, ok := response.(errorer); ok && e.error() != nil {
		encodeError(ctx, e.error(), w)
		return nil
	}
	w.Header().Set("Content-Type", "application/json; charset=utf-8")
	return json.NewEncoder(w).Encode(response)
}

type errorer interface {
	error() error
}

// encode errors from business-logic
func encodeError(_ context.Context, err error, w http.ResponseWriter) {
	w.Header().Set("Content-Type", "application/json; charset=utf-8")

	msg := "internal error"
	status := http.StatusInternalServerError

	switch err.(type) {
	case *ErrInvalidArg:
		status = http.StatusBadRequest
	case *ErrNotFound:
		status = http.StatusNotFound

	}

	// hide internal errors from API users
	if status != http.StatusInternalServerError {
		msg = err.Error()
	}

	w.WriteHeader(status)

	json.NewEncoder(w).Encode(map[string]interface{}{
		"error": msg,
	})
}
