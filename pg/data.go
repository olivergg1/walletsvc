// Package pg implements account and payment's stores via PostgreSQL db.
package pg

import (
	"bitbucket.org/kovalevm/walletsvc/account"
	"bitbucket.org/kovalevm/walletsvc/payment"
)

// DB entity of the account.
type accountRow struct {
	Id string `db:"id"`
	// Store money in integer for best performance
	Balance uint64 `db:"balance"`

	Currency string `db:"currency"`
}

// Converts db entity to main account entity.
func (th *accountRow) ToAccount() *account.Account {
	return &account.Account{
		Id:       th.Id,
		Balance:  fromMicrodollar(th.Balance),
		Currency: th.Currency,
	}
}

// DB entity of the payment.
type paymentRow struct {
	Id uint32 `db:"id"`
	// Store money in integer for best performance
	Account string `db:"account"`

	Amount    uint64 `db:"amount"`
	ToAccount string `db:"to_account"`
	Direction string `db:"direction"`
}

// Converts main payment entity to db entity.
func newPaymentRow(p *payment.Payment) *paymentRow {
	return &paymentRow{
		Account:   p.Account,
		Amount:    toMicrodollar(p.Amount),
		ToAccount: p.ToAccount,
		Direction: p.Direction,
	}
}

// Converts db entity to main payment entity.
func (th *paymentRow) ToPayment() *payment.Payment {
	return &payment.Payment{
		Account:   th.Account,
		Amount:    fromMicrodollar(th.Amount),
		ToAccount: th.ToAccount,
		Direction: th.Direction,
	}
}
