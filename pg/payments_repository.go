// Package pg implements account and payment's stores via PostgreSQL db.
package pg

import (
	"bitbucket.org/kovalevm/walletsvc/payment"
	"context"
	"database/sql"
	"fmt"
	"github.com/jmoiron/sqlx"
)

// Payment's store scheme is used for tests.
const PAYMENTS_SCHEME = `
create table if not exists payment (
    id         serial      not null constraint payment_pkey primary key,
    account    varchar(50) not null constraint payment_account_fkey references account,
    amount     bigint      not null,
    to_account varchar(50) not null constraint payment_to_account_fkey references account,
    direction  varchar(50) not null,
    constraint payment_check check ((account)::text <> (to_account)::text)
);
`

// Instance to implement payments repository.
type paymentsRepository struct {
	db *sqlx.DB
}

// Returns a new instance of a PostgreSQL payments repository.
func NewPaymentsRepository(db *sqlx.DB) payment.Repository {
	return &paymentsRepository{db}
}

// Returns a list of stored payments.
func (th *paymentsRepository) FindAll(limit, offset int) ([]*payment.Payment, error) {
	var rows []*paymentRow
	if err := th.db.Select(&rows, fmt.Sprintf("SELECT * FROM payment LIMIT %d OFFSET %d", limit, offset)); err != nil {
		return nil, err
	}

	var payments []*payment.Payment
	for _, v := range rows {
		payments = append(payments, v.ToPayment())
	}

	return payments, nil
}

// Transfers money between accounts and store payment entity.
func (th *paymentsRepository) Send(ctx context.Context, p *payment.Payment) error {
	increaseId := p.AccountToIncrease()
	decreaseId := p.AccountToDecrease()
	row := newPaymentRow(p)

	return th.Transact(ctx, nil, func(tx *sqlx.Tx) error {
		decreaseAcc := accountRow{}
		// SELECT ... FOR UPDATE - lock a row until transaction is commit/rollback.
		if err := tx.Get(&decreaseAcc, "SELECT * FROM account WHERE id = $1 FOR UPDATE", decreaseId); err != nil {
			return err
		}

		if decreaseAcc.Balance < row.Amount {
			return payment.ErrNotEnoughMoney
		}

		// Update two balances in one query
		_, err := tx.NamedExec(
			fmt.Sprintf(
				`UPDATE account SET balance = balance + v.amount
			FROM (VALUES (:decrease_id, -%d), (:increase_id, %d)) AS v(id, amount)
			WHERE account.id = v.id`, row.Amount, row.Amount),
			map[string]interface{}{"decrease_id": decreaseId, "increase_id": increaseId},
		)
		if err != nil {
			return err
		}

		_, err = tx.NamedExec(
			`INSERT INTO payment (account, amount, to_account, direction)
			 VALUES (:account, :amount, :to_account, :direction)`,
			row,
		)
		if err != nil {
			return err
		}

		return nil
	})
}

// Transact executes a function where transaction atomicity on the database is guaranteed.
// If the function is successfully completed, the changes are committed to the database.
// If there is an error, the changes are rolled back.
func (th *paymentsRepository) Transact(ctx context.Context, txOptions *sql.TxOptions, atomic func(*sqlx.Tx) error) (err error) {
	tx, err := th.db.BeginTxx(ctx, txOptions)
	if err != nil {
		return
	}
	defer func() {
		// Catch panics to ensure a Rollback happens right away.
		// Under normal circumstances a panic should not occur.
		// If we did not handle panics, the transaction would be rolled back eventually.
		// A non-committed transaction gets rolled back by the database when the client disconnects
		// or when the transaction gets garbage collected.
		// It's better to resolve the issue as quickly as possible.
		if p := recover(); p != nil {
			tx.Rollback()
			panic(p)
		}
		// err is not nil; don't change it.
		if err != nil {
			tx.Rollback()
			return
		}
		// err is nil; if Commit returns error, update err.
		err = tx.Commit()
	}()

	err = atomic(tx)
	return err
}
