// Package pg implements account and payment's stores via PostgreSQL db.
package pg

import (
	"bitbucket.org/kovalevm/walletsvc/account"
	"database/sql"
	"fmt"
	"github.com/jmoiron/sqlx"
)

// Account's store scheme is used for tests.
const ACCOUNTS_SCHEME = `
create table if not exists account (
id       varchar(50) not null constraint account_pkey primary key,
balance  bigint      not null constraint account_balance_check check (balance >= 0),
currency varchar(3)  not null
)
`

// Instance to implement accounts repository.
type accountsRepository struct {
	db *sqlx.DB
}

// Returns a new instance of a PostgreSQL accounts repository.
func NewAccountsRepository(db *sqlx.DB) account.Repository {
	return &accountsRepository{db}
}

// Returns account with passed id.
func (th *accountsRepository) Find(id string) (*account.Account, error) {
	accountRow := accountRow{}
	if err := th.db.Get(&accountRow, "SELECT * FROM account WHERE id=$1", id); err == sql.ErrNoRows {
		return nil, account.ErrUnknown
	} else if err != nil {
		return nil, err
	}

	return accountRow.ToAccount(), nil
}

// Returns a list of stored accounts.
func (th *accountsRepository) FindAll(limit, offset int) ([]*account.Account, error) {
	var rows []*accountRow
	if err := th.db.Select(&rows, fmt.Sprintf("SELECT * FROM account LIMIT %d OFFSET %d", limit, offset)); err != nil {
		return nil, err
	}

	var accounts []*account.Account
	for _, v := range rows {
		accounts = append(accounts, v.ToAccount())
	}

	return accounts, nil
}
