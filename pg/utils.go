// Package pg implements account and payment's stores via PostgreSQL db.
package pg

import "math"

const microdollarRatio = 1000000

// Converts db balance to real balance.
func fromMicrodollar(value uint64) float64 {
	actual := float64(value) / microdollarRatio
	// Round float down to 2 decimal places.
	// Down because we can't give more than we have.
	return math.Floor(actual*100) / 100

}

// Converts real balance to db balance.
func toMicrodollar(value float64) uint64 {
	f := value * microdollarRatio

	return uint64(f)
}
