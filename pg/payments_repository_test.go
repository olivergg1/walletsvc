// Package pg implements account and payment's stores via PostgreSQL db.
package pg

import (
	"bitbucket.org/kovalevm/walletsvc/payment"
	"context"
	_ "github.com/jackc/pgx/stdlib"
	"github.com/jmoiron/sqlx"
	"github.com/stretchr/testify/suite"
	"testing"
)

type PaymentsRepositoryTestSuite struct {
	suite.Suite
	db   *sqlx.DB
	repo payment.Repository
}

// Executes before each test.
func (th *PaymentsRepositoryTestSuite) SetupTest() {
	pgConnectionString := envString("DATABASE_URL", "postgresql://postgres:postgres@postgres/test")

	th.db = sqlx.MustConnect("pgx", pgConnectionString)

	th.db.MustExec(ACCOUNTS_SCHEME)
	th.db.MustExec(PAYMENTS_SCHEME)

	insertAccounts(
		th.db,
		&accountRow{Id: "tony", Balance: 990000000, Currency: "USD"},
		&accountRow{Id: "adam", Balance: 5000000, Currency: "USD"},
	)

	th.repo = NewPaymentsRepository(th.db) //.(*accountsRepository)
}

// Executes after each test.
func (th *PaymentsRepositoryTestSuite) TearDownTest() {
	th.db.MustExec("DROP TABLE if exists payment cascade")
	th.db.MustExec("DROP TABLE if exists account cascade")
}

// Should catch DB error because LIMIT AND OFFSET can't be negative.
func (th *PaymentsRepositoryTestSuite) TestFindAllErr() {
	_, err := th.repo.FindAll(-10, 0)
	th.Require().Error(err)

	_, err = th.repo.FindAll(10, -99)
	th.Require().Error(err)
}

// Should get empty result because no one payment row is inserted.
func (th *PaymentsRepositoryTestSuite) TestFindAllEmpty() {
	var expected []*payment.Payment
	actual, err := th.repo.FindAll(100, 0)
	th.Require().NoError(err)
	th.Require().Equal(expected, actual)
}

// Should get empty result because LIMIT is 0.
func (th *PaymentsRepositoryTestSuite) TestFindAllEmptyByLimit() {
	var expected []*payment.Payment
	row := &paymentRow{Account: "tony", Amount: 2000000, ToAccount: "adam", Direction: "outgoing"}
	th.insert(row)

	actual, err := th.repo.FindAll(0, 0)
	th.Require().NoError(err)
	th.Require().Equal(expected, actual)
}

// Should get empty result because OFFSET more or equal than rows in table.
func (th *PaymentsRepositoryTestSuite) TestFindAllEmptyByOffset() {
	var expected []*payment.Payment
	row := &paymentRow{Account: "tony", Amount: 2000000, ToAccount: "adam", Direction: "outgoing"}
	th.insert(row)

	actual, err := th.repo.FindAll(100, 1)
	th.Require().NoError(err)
	th.Require().Equal(expected, actual)
}

// Should check inserted payments is the same than extracted payments.
func (th *PaymentsRepositoryTestSuite) TestFindAll() {
	row1 := &paymentRow{Account: "tony", Amount: 2000000, ToAccount: "adam", Direction: "outgoing"}
	row2 := &paymentRow{Account: "adam", Amount: 10000000, ToAccount: "tony", Direction: "incoming"}
	th.insert(row1, row2)

	var expected []*payment.Payment
	expected = append(expected, row1.ToPayment())
	expected = append(expected, row2.ToPayment())

	actual, err := th.repo.FindAll(100, 0)
	th.Require().NoError(err)
	th.Require().Equal(expected, actual)
}

// Should catch ErrNotEnoughMoney error because account's balance less than payment's amount.
func (th *PaymentsRepositoryTestSuite) TestSendNotEnoughMoney() {
	row := &paymentRow{Account: "adam", Amount: 9910000000, ToAccount: "tony", Direction: "incoming"}
	err := th.repo.Send(context.Background(), row.ToPayment())
	th.Require().Error(err, payment.ErrNotEnoughMoney)
}

// Checks that payment is sent successfully.
func (th *PaymentsRepositoryTestSuite) TestSend() {
	var err error
	paymentRow := &paymentRow{Account: "tony", Amount: 2000000, ToAccount: "adam", Direction: "incoming"}
	p := paymentRow.ToPayment()

	decreaseAccRowBefore := accountRow{}
	err = th.db.Get(&decreaseAccRowBefore, "SELECT * FROM account WHERE id=$1", paymentRow.Account)
	th.Require().NoError(err)

	increaseAccRowBefore := accountRow{}
	err = th.db.Get(&increaseAccRowBefore, "SELECT * FROM account WHERE id=$1", paymentRow.ToAccount)
	th.Require().NoError(err)

	err = th.repo.Send(context.Background(), p)
	th.Require().NoError(err)

	decreaseAccRowAfter := accountRow{}
	err = th.db.Get(&decreaseAccRowAfter, "SELECT * FROM account WHERE id=$1", paymentRow.Account)
	th.Require().NoError(err)
	th.Require().Equal(decreaseAccRowBefore.Balance-paymentRow.Amount, decreaseAccRowAfter.Balance)

	increaseAccRowAfter := accountRow{}
	err = th.db.Get(&increaseAccRowAfter, "SELECT * FROM account WHERE id=$1", paymentRow.ToAccount)
	th.Require().NoError(err)
	th.Require().Equal(increaseAccRowBefore.Balance+paymentRow.Amount, increaseAccRowAfter.Balance)
}

// Checks that outgoing payment is sent successfully.
func (th *PaymentsRepositoryTestSuite) TestSendOutgoing() {
	var err error
	paymentRow := &paymentRow{Account: "tony", Amount: 2000000, ToAccount: "adam", Direction: "outgoing"}
	p := paymentRow.ToPayment()

	decreaseAccRowBefore := accountRow{}
	err = th.db.Get(&decreaseAccRowBefore, "SELECT * FROM account WHERE id=$1", paymentRow.ToAccount)
	th.Require().NoError(err)

	increaseAccRowBefore := accountRow{}
	err = th.db.Get(&increaseAccRowBefore, "SELECT * FROM account WHERE id=$1", paymentRow.Account)
	th.Require().NoError(err)

	err = th.repo.Send(context.Background(), p)
	th.Require().NoError(err)

	decreaseAccRowAfter := accountRow{}
	err = th.db.Get(&decreaseAccRowAfter, "SELECT * FROM account WHERE id=$1", paymentRow.ToAccount)
	th.Require().NoError(err)
	th.Require().Equal(decreaseAccRowBefore.Balance-paymentRow.Amount, decreaseAccRowAfter.Balance)

	increaseAccRowAfter := accountRow{}
	err = th.db.Get(&increaseAccRowAfter, "SELECT * FROM account WHERE id=$1", paymentRow.Account)
	th.Require().NoError(err)
	th.Require().Equal(increaseAccRowBefore.Balance+paymentRow.Amount, increaseAccRowAfter.Balance)
}

func (th *PaymentsRepositoryTestSuite) insert(rows ...*paymentRow) {
	for _, row := range rows {
		_, err := th.db.NamedExec(
			`INSERT INTO payment (account, amount, to_account, direction)
			 VALUES (:account, :amount, :to_account, :direction)`,
			row,
		)
		if err != nil {
			panic(err)
		}
	}
}

func TestPaymentsRepositoryTestSuite(t *testing.T) {
	suite.Run(t, new(PaymentsRepositoryTestSuite))
}
