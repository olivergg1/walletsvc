// Package pg implements account and payment's stores via PostgreSQL db.
package pg

import (
	"bitbucket.org/kovalevm/walletsvc/account"
	_ "github.com/jackc/pgx/stdlib"
	"github.com/jmoiron/sqlx"
	"github.com/stretchr/testify/suite"
	"os"
	"testing"
)

type AccountsRepositoryTestSuite struct {
	suite.Suite
	db   *sqlx.DB
	repo account.Repository
}

// Executes before each test.
func (th *AccountsRepositoryTestSuite) SetupTest() {
	pgConnectionString := envString("DATABASE_URL", "postgresql://postgres:postgres@postgres/test")

	th.db = sqlx.MustConnect("pgx", pgConnectionString)

	th.db.MustExec(ACCOUNTS_SCHEME)

	th.repo = NewAccountsRepository(th.db)
}

// Executes after each test.
func (th *AccountsRepositoryTestSuite) TearDownTest() {
	th.db.MustExec("DROP TABLE if exists account cascade")
}

// Should catch the account.ErrUnknown because no one account row is inserted.
func (th *AccountsRepositoryTestSuite) TestFindNotFound() {
	_, err := th.repo.Find("tony")
	th.Require().Error(err, account.ErrUnknown)
}

// Should check inserted account is the same than extracted account.
func (th *AccountsRepositoryTestSuite) TestFind() {
	row := &accountRow{Id: "tony", Balance: 2000000, Currency: "USD"}
	insertAccounts(th.db, row)

	exp := row.ToAccount()
	actual, err := th.repo.Find(exp.Id)
	th.Require().NoError(err)
	th.Require().Equal(exp, actual)
}

// Should catch DB error because LIMIT AND OFFSET can't be negative.
func (th *AccountsRepositoryTestSuite) TestFindAllErr() {
	_, err := th.repo.FindAll(-10, 0)
	th.Require().Error(err)

	_, err = th.repo.FindAll(10, -99)
	th.Require().Error(err)
}

// Should get empty result because no one account row is inserted.
func (th *AccountsRepositoryTestSuite) TestFindAllEmpty() {
	var expected []*account.Account
	actual, err := th.repo.FindAll(100, 0)
	th.Require().NoError(err)
	th.Require().Equal(expected, actual)
}

// Should get empty result because LIMIT is 0.
func (th *AccountsRepositoryTestSuite) TestFindAllEmptyByLimit() {
	var expected []*account.Account

	row := &accountRow{Id: "tony", Balance: 2000000, Currency: "USD"}
	insertAccounts(th.db, row)

	actual, err := th.repo.FindAll(0, 0)
	th.Require().NoError(err)
	th.Require().Equal(expected, actual)
}

// Should get empty result because OFFSET more or equal than rows in table.
func (th *AccountsRepositoryTestSuite) TestFindAllEmptyByOffset() {
	var expected []*account.Account

	row := &accountRow{Id: "tony", Balance: 2000000, Currency: "USD"}
	insertAccounts(th.db, row)

	actual, err := th.repo.FindAll(100, 1)
	th.Require().NoError(err)
	th.Require().Equal(expected, actual)
}

// Should check inserted accounts is the same than extracted accounts.
func (th *AccountsRepositoryTestSuite) TestFindAll() {
	row1 := &accountRow{Id: "conor", Balance: 990000000, Currency: "USD"}
	row2 := &accountRow{Id: "habib", Balance: 5000000, Currency: "USD"}
	insertAccounts(th.db, row1, row2)

	var expected []*account.Account
	expected = append(expected, row1.ToAccount())
	expected = append(expected, row2.ToAccount())

	actual, err := th.repo.FindAll(100, 0)
	th.Require().NoError(err)
	th.Require().Equal(expected, actual)
}

// Used in another tests too.
func insertAccounts(db *sqlx.DB, rows ...*accountRow) {
	for _, row := range rows {
		db.MustExec("INSERT INTO account (id, balance, currency) VALUES ($1, $2, $3)", row.Id, row.Balance, row.Currency)
	}
}

func TestAccountsRepositoryTestSuite(t *testing.T) {
	suite.Run(t, new(AccountsRepositoryTestSuite))
}

func envString(env, fallback string) string {
	e := os.Getenv(env)
	if e == "" {
		return fallback
	}
	return e
}
