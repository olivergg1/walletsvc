FROM registry.orbita1.ru/docker/golang:latest as builder
MAINTAINER Mikhail Kovalev <kovalev.mixail@gmail.com>
ADD . /go/src/bitbucket.org/kovalevm/walletsvc
WORKDIR /go/src/bitbucket.org/kovalevm/walletsvc
RUN make build

FROM alpine:latest
MAINTAINER Mikhail Kovalev <kovalev.mixail@gmail.com>
RUN apk --no-cache add ca-certificates
WORKDIR /walletsvc/
COPY --from=builder /go/src/bitbucket.org/kovalevm/walletsvc/dist/walletsvcd .
CMD ["./walletsvcd"]
