# 'help' target by default
.PHONY: default test
default: help

## Download dependencies
deps:
	go get -d ./...

## Remove dist/ dir
clean:
	rm -rf ./dist

## Compile daemon into dist/
compile:
	mkdir -p ./dist
	rm -f ./dist/walletsvcd
	CGO_ENABLED=0 GOOS=linux go build -a -o ./dist/walletsvcd ./cmd/walletsvc/main.go

## Run tests
test: deps
	go test -v ./...

## Run 'deps' + 'compile'
build: deps compile

## This help screen
help:
	$(info Available targets)
	@awk '/^[a-zA-Z\-\_0-9\.]+:/ { \
		helpMessage = match(lastLine, /^## (.*)/); \
		if (helpMessage) { \
			helpCommand = substr($$1, 0, index($$1, ":")); \
			helpMessage = substr(lastLine, RSTART + 3, RLENGTH); \
			printf "\033[1;32m %-20s \033[0m %s\n", helpCommand, helpMessage; \
		} \
	} \
	{ lastLine = $$0 }' $(MAKEFILE_LIST)
