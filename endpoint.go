// endpoint.go collects all of the endpoints that compose a wallet service.
package walletsvc

import (
	"bitbucket.org/kovalevm/walletsvc/account"
	"bitbucket.org/kovalevm/walletsvc/payment"
	"context"
	"github.com/go-kit/kit/endpoint"
)

// Provides params for list handlers.
type listRequest struct {
	Limit  int
	Offset int
}

type listAccountsResponse struct {
	Accounts []*account.Account `json:"accounts,omitempty"`
	Err      error              `json:"error,omitempty"`
}

func (r listAccountsResponse) error() error { return r.Err }

// Returns a list of accounts endpoint via the passed service.
func makeListAccountsEndpoint(s Service) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		lr := request.(listRequest)

		accounts, err := s.Accounts(&lr)
		if err != nil {
			return listAccountsResponse{Accounts: nil, Err: err}, nil
		}

		return listAccountsResponse{Accounts: accounts, Err: nil}, nil
	}
}

type listPaymentsResponse struct {
	Payments []*payment.Payment `json:"payments,omitempty"`
	Err      error              `json:"error,omitempty"`
}

func (r listPaymentsResponse) error() error { return r.Err }

// Returns a list of payments endpoint via the passed service.
func makeListPaymentsEndpoint(s Service) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		lr := request.(listRequest)

		payments, err := s.Payments(&lr)
		if err != nil {
			return listPaymentsResponse{Payments: nil, Err: err}, nil
		}

		return listPaymentsResponse{Payments: payments, Err: nil}, nil
	}
}

type sendPaymentResponse struct {
	Err error `json:"error,omitempty"`
}

func (r sendPaymentResponse) error() error { return r.Err }

// Returns a send a payment endpoint via the passed service.
func makeSendPaymentEndpoint(s Service) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		// use payment.Payment as entity to describe request fields
		req := request.(*payment.Payment)

		err := s.SendPayment(ctx, req)

		return sendPaymentResponse{Err: err}, nil
	}
}
