package walletsvc

import (
	"bitbucket.org/kovalevm/walletsvc/account"
	"bitbucket.org/kovalevm/walletsvc/payment"
	"context"
	"fmt"
)

// ErrInvalidArg is used when one or more incoming arguments are incorrect.
type ErrInvalidArg struct {
	error
}

func NewErrInvalidArg(err error) *ErrInvalidArg {
	return &ErrInvalidArg{err}
}

// ErrNotFound is used when requested entity can't be found.
type ErrNotFound struct {
	error
}

func NewErrNotFound(err error) *ErrNotFound {
	return &ErrNotFound{err}
}

// Providers validation of incoming credentials.
type validationService struct {
	accounts account.Repository
	Service
}

// NewValidationService returns a new instance of a validation Service.
func NewValidationService(accounts account.Repository, s Service) Service {
	return &validationService{accounts, s}
}

func (s *validationService) Accounts(lr *listRequest) ([]*account.Account, error) {
	if err := s.validateListRequest(lr); err != nil {
		return nil, NewErrInvalidArg(err)
	}

	return s.Service.Accounts(lr)
}

func (s *validationService) Payments(lr *listRequest) ([]*payment.Payment, error) {
	if err := s.validateListRequest(lr); err != nil {
		return nil, NewErrInvalidArg(err)
	}

	return s.Service.Payments(lr)
}

func (s *validationService) validateListRequest(lr *listRequest) error {
	if lr.Limit <= 0 {
		return NewErrInvalidArg(fmt.Errorf("invalid 'limit' value, should be positive number"))
	}

	if lr.Offset < 0 {
		return NewErrInvalidArg(fmt.Errorf("invalid 'offset' value, should be non-negative number"))
	}

	return nil
}

func (s *validationService) SendPayment(ctx context.Context, p *payment.Payment) error {
	if p.Account == "" || p.ToAccount == "" {
		return NewErrInvalidArg(fmt.Errorf("'account' and 'to_account' args can't be empty"))
	}

	if p.Account == p.ToAccount {
		return NewErrInvalidArg(fmt.Errorf("'account' and 'to_account' can't be same"))
	}

	if p.Amount <= 0 {
		return NewErrInvalidArg(fmt.Errorf("'amount' arg is incorrect: should be positive number"))
	}

	if p.Direction != payment.DIRECTION_INCOMING && p.Direction != payment.DIRECTION_OUTGOING {
		return NewErrInvalidArg(fmt.Errorf(
			"'direction' arg is incorrect: avalible options: ['%s', '%s']",
			payment.DIRECTION_INCOMING,
			payment.DIRECTION_OUTGOING,
		))
	}

	acc, err := s.accounts.Find(p.AccountToDecrease())
	if err == account.ErrUnknown {
		return NewErrNotFound(err)
	} else if err != nil {
		return err
	}

	if acc.Balance < p.Amount {
		return NewErrInvalidArg(fmt.Errorf("'amount' arg is too high: %s", payment.ErrNotEnoughMoney))
	}

	toAcc, err := s.accounts.Find(p.AccountToIncrease())
	if err == account.ErrUnknown {
		return NewErrNotFound(err)
	} else if err != nil {
		return err
	}

	if acc.Currency != toAcc.Currency {
		return NewErrInvalidArg(fmt.Errorf("accounts have different currency"))
	}

	return s.Service.SendPayment(ctx, p)
}
