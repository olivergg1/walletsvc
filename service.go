package walletsvc

import (
	"bitbucket.org/kovalevm/walletsvc/account"
	"bitbucket.org/kovalevm/walletsvc/payment"
	"context"
)

// Service is the interface that provides wallet methods.
type Service interface {
	// Accounts returns a list of all accounts that have been stored.
	Accounts(lr *listRequest) ([]*account.Account, error)

	// Payments returns a list of all payments that have been stored.
	Payments(lr *listRequest) ([]*payment.Payment, error)

	// SendPayment transfers money for one account to another.
	SendPayment(ctx context.Context, p *payment.Payment) error
}

type service struct {
	accounts account.Repository
	payments payment.Repository
}

func (s *service) Accounts(lr *listRequest) ([]*account.Account, error) {
	return s.accounts.FindAll(lr.Limit, lr.Offset)
}

func (s *service) Payments(lr *listRequest) ([]*payment.Payment, error) {
	return s.payments.FindAll(lr.Limit, lr.Offset)
}

func (s *service) SendPayment(ctx context.Context, p *payment.Payment) error {
	return s.payments.Send(ctx, p)
}

// NewService creates a wallet service with necessary dependencies.
func NewService(accounts account.Repository, payments payment.Repository) Service {
	return &service{
		accounts: accounts,
		payments: payments,
	}
}
