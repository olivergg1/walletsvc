// Package account contains the heart of the domain model.
package account

import (
	"errors"
)

// Account is the central class in the domain model.
type Account struct {
	Id       string  `json:"id"`
	Balance  float64 `json:"balance"`
	Currency string  `json:"currency"`
}

// ErrUnknown is used when an account could not be found.
var ErrUnknown = errors.New("unknown account")

// Repository provides access an account store.
type Repository interface {
	// Returns account with passed id.
	Find(id string) (*Account, error)

	// Returns a list of stored accounts.
	FindAll(limit, offset int) ([]*Account, error)
}
