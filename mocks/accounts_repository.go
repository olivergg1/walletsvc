// Package mocks provides imitations of app instances for unit testing.
package mocks

import (
	"bitbucket.org/kovalevm/walletsvc/account"
	"github.com/stretchr/testify/mock"
)

type AccountsRepositoryMock struct {
	mock.Mock
}

func (th *AccountsRepositoryMock) Find(id string) (*account.Account, error) {
	args := th.Called(id)

	return args.Get(0).(*account.Account), args.Error(1)
}

func (th *AccountsRepositoryMock) FindAll(limit, offset int) ([]*account.Account, error) {
	args := th.Called(limit, offset)

	return args.Get(0).([]*account.Account), args.Error(1)
}
