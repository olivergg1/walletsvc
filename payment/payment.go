// Package payment provides the transfers between accounts.
package payment

import (
	"context"
	"errors"
)

// 'incoming' direction means money should be transfer from Account to ToAccount.
const DIRECTION_INCOMING = "incoming"

// 'outgoing' direction means money should be transfer from ToAccount to Account.
const DIRECTION_OUTGOING = "outgoing"

// Payment is an entity to describe transaction between accounts.
type Payment struct {
	Account   string  `json:"account"`
	Amount    float64 `json:"amount"`
	ToAccount string  `json:"to_account"`
	Direction string  `json:"direction"`
}

// Returns account's ID for receiving a money.
func (th *Payment) AccountToIncrease() string {
	if th.Direction == DIRECTION_OUTGOING {
		return th.Account
	}

	return th.ToAccount
}

// Returns account's ID for taking money.
func (th *Payment) AccountToDecrease() string {
	if th.Direction == DIRECTION_OUTGOING {
		return th.ToAccount
	}

	return th.Account
}

// ErrUnknown is used when a payment could not be found.
var ErrUnknown = errors.New("unknown payment")

// ErrNotEnoughMoney is used when a payment is not possible because of not enough money on taking money balance.
var ErrNotEnoughMoney = errors.New("not enough money")

// Repository provides access a payment store.
type Repository interface {
	// Returns a list of stored payments.
	FindAll(limit, offset int) ([]*Payment, error)

	// Transfers money between accounts and store payment entity.
	Send(ctx context.Context, p *Payment) error
}
