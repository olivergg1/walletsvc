package walletsvc

import (
	"bitbucket.org/kovalevm/walletsvc/account"
	"bitbucket.org/kovalevm/walletsvc/payment"
	"context"
	"time"

	"github.com/go-kit/kit/metrics"
)

// Provides metrics for endpoints
type instrumentingService struct {
	requestCount   metrics.Counter
	requestLatency metrics.Histogram
	Service
}

// NewInstrumentingService returns an instance of an instrumenting Service.
func NewInstrumentingService(counter metrics.Counter, latency metrics.Histogram, s Service) Service {
	return &instrumentingService{
		requestCount:   counter,
		requestLatency: latency,
		Service:        s,
	}
}

func (s *instrumentingService) Accounts(lr *listRequest) ([]*account.Account, error) {
	defer func(begin time.Time) {
		s.requestCount.With("method", "list_accounts").Add(1)
		s.requestLatency.With("method", "list_accounts").Observe(time.Since(begin).Seconds())
	}(time.Now())

	return s.Service.Accounts(lr)
}

func (s *instrumentingService) Payments(lr *listRequest) ([]*payment.Payment, error) {
	defer func(begin time.Time) {
		s.requestCount.With("method", "list_payments").Add(1)
		s.requestLatency.With("method", "list_payments").Observe(time.Since(begin).Seconds())
	}(time.Now())

	return s.Service.Payments(lr)
}

func (s *instrumentingService) SendPayment(ctx context.Context, p *payment.Payment) error {
	defer func(begin time.Time) {
		s.requestCount.With("method", "send_payment").Add(1)
		s.requestLatency.With("method", "send_payment").Observe(time.Since(begin).Seconds())
	}(time.Now())

	return s.Service.SendPayment(ctx, p)
}
