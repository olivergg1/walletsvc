# walletsvc
Wallet service with RESTful API.

## Description
Provides API for the following user stories:

* I want to be able to send a payment from one account to another (same currency)
* I want to be able to see all payments
* I want to be able to see available accounts

[API docs here](docs/api.md)

## Assumptions and requirements
* Only payments within the same currency are supported (no exchanges)
* There are no users in the system
* Balance can't go below zero
* More than one instance of the application can be launched

## Persistent storage
Service uses PostrgesSQL as persistent storage. Make sure postrges server is available. 

## Build
```
make build
```

## Settings
The following environment variables can be used to manage the service settings:

* `PORT` - HTTP listen address. By default - `8081`
* `DATABASE_URL` - database URL. By default - `postgresql://postgres:postgres@localhost/coins`

## Continuous integration and delivery 
To run tests in docker env:
```
docker-compose run --rm app make test
```
Then, you can create a container with the service 
and push it to you registry(or docker hub)
```
docker build -t kovavelvm/walletsvc:latest .
docker push kovavelvm/walletsvc:latest
```

To run on production server, as example:
```
docker pull kovavelvm/walletsvc:latest
docker stop walletsvc
docker rm --volumes walletsvc 

export PORT=8080
export DATABASE_URL="postgresql://user:password@host/db"

docker run -d \
    --net=host \
    --name=walletsvc \
    --env PORT \
    --env DATABASE_URL \
    kovavelvm/walletsvc:latest
    
```
