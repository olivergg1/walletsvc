package walletsvc

import (
	"bitbucket.org/kovalevm/walletsvc/account"
	"bitbucket.org/kovalevm/walletsvc/mocks"
	"bitbucket.org/kovalevm/walletsvc/payment"
	"context"
	"fmt"
	"github.com/stretchr/testify/suite"
	"testing"
)

type ValidationServiceTestSuite struct {
	suite.Suite
	accountsMock *mocks.AccountsRepositoryMock
	s            Service
}

// Executes before each test.
func (th *ValidationServiceTestSuite) SetupTest() {
	th.accountsMock = new(mocks.AccountsRepositoryMock)

	var s Service
	th.s = NewValidationService(th.accountsMock, s)
}

// Should catch ErrInvalidArg error because of incorrect limit and offset.
func (th *ValidationServiceTestSuite) TestAccounts() {
	lr := &listRequest{Limit: -10, Offset: 0}
	_, err := th.s.Accounts(lr)
	th.Require().Error(err)
	th.Require().IsType((*ErrInvalidArg)(nil), err)

	lr = &listRequest{Limit: 0, Offset: 0}
	_, err = th.s.Accounts(lr)
	th.Require().Error(err)
	th.Require().IsType((*ErrInvalidArg)(nil), err)

	lr = &listRequest{Limit: 100, Offset: -90}
	_, err = th.s.Accounts(lr)
	th.Require().Error(err)
	th.Require().IsType((*ErrInvalidArg)(nil), err)
}

// Should catch ErrInvalidArg error because of incorrect limit and offset.
func (th *ValidationServiceTestSuite) TestPayments() {
	lr := &listRequest{Limit: -10, Offset: 0}
	_, err := th.s.Payments(lr)
	th.Require().Error(err)
	th.Require().IsType((*ErrInvalidArg)(nil), err)

	lr = &listRequest{Limit: 0, Offset: 0}
	_, err = th.s.Payments(lr)
	th.Require().Error(err)
	th.Require().IsType((*ErrInvalidArg)(nil), err)

	lr = &listRequest{Limit: 100, Offset: -90}
	_, err = th.s.Payments(lr)
	th.Require().Error(err)
	th.Require().IsType((*ErrInvalidArg)(nil), err)
}

// A kit of invalid payment parameters tests.
func (th *ValidationServiceTestSuite) TestSendPayment() {
	p := &payment.Payment{}
	err := th.s.SendPayment(context.Background(), p)
	th.Require().Error(err)
	th.Require().IsType((*ErrInvalidArg)(nil), err)

	p = &payment.Payment{Account: "", Amount: 2, ToAccount: "tony", Direction: payment.DIRECTION_INCOMING}
	err = th.s.SendPayment(context.Background(), p)
	th.Require().Error(err)
	th.Require().IsType((*ErrInvalidArg)(nil), err)

	p = &payment.Payment{Account: "adam", Amount: 2, ToAccount: "", Direction: payment.DIRECTION_INCOMING}
	err = th.s.SendPayment(context.Background(), p)
	th.Require().Error(err)
	th.Require().IsType((*ErrInvalidArg)(nil), err)

	p = &payment.Payment{Account: "adam", Amount: 2, ToAccount: "adam", Direction: payment.DIRECTION_INCOMING}
	err = th.s.SendPayment(context.Background(), p)
	th.Require().Error(err)
	th.Require().IsType((*ErrInvalidArg)(nil), err)

	p = &payment.Payment{Account: "adam", Amount: -2, ToAccount: "tony", Direction: payment.DIRECTION_INCOMING}
	err = th.s.SendPayment(context.Background(), p)
	th.Require().Error(err)
	th.Require().IsType((*ErrInvalidArg)(nil), err)

	p = &payment.Payment{Account: "adam", Amount: 0, ToAccount: "tony", Direction: payment.DIRECTION_INCOMING}
	err = th.s.SendPayment(context.Background(), p)
	th.Require().Error(err)
	th.Require().IsType((*ErrInvalidArg)(nil), err)

	p = &payment.Payment{Account: "adam", Amount: 1, ToAccount: "tony", Direction: ""}
	err = th.s.SendPayment(context.Background(), p)
	th.Require().Error(err)
	th.Require().IsType((*ErrInvalidArg)(nil), err)

	p = &payment.Payment{Account: "adam", Amount: 1, ToAccount: "tony", Direction: "unknown_direction"}
	err = th.s.SendPayment(context.Background(), p)
	th.Require().Error(err)
	th.Require().IsType((*ErrInvalidArg)(nil), err)

	p = &payment.Payment{Account: "adam", Amount: 100, ToAccount: "tony", Direction: payment.DIRECTION_OUTGOING}
	th.accountsMock.On("Find", p.ToAccount).Return(&account.Account{}, account.ErrUnknown).Once()
	err = th.s.SendPayment(context.Background(), p)
	th.Require().EqualError(err, account.ErrUnknown.Error())

	p = &payment.Payment{Account: "adam", Amount: 100, ToAccount: "tony", Direction: payment.DIRECTION_INCOMING}

	th.accountsMock.On("Find", p.Account).Return(&account.Account{}, account.ErrUnknown).Once()
	err = th.s.SendPayment(context.Background(), p)
	th.Require().EqualError(err, account.ErrUnknown.Error())

	th.accountsMock.On("Find", p.Account).Return(&account.Account{}, fmt.Errorf("sql error")).Once()
	err = th.s.SendPayment(context.Background(), p)
	th.Require().Error(err)
	th.Require().Contains(err.Error(), "sql error")

	th.accountsMock.On("Find", p.Account).Return(&account.Account{Balance: 5}, nil).Once()
	err = th.s.SendPayment(context.Background(), p)
	th.Require().Error(err)
	th.Require().Contains(err.Error(), payment.ErrNotEnoughMoney.Error())

	th.accountsMock.On("Find", p.Account).Return(&account.Account{Balance:200}, nil).Once()
	th.accountsMock.On("Find", p.ToAccount).Return(&account.Account{}, account.ErrUnknown).Once()
	err = th.s.SendPayment(context.Background(), p)
	th.Require().EqualError(err, account.ErrUnknown.Error())

	th.accountsMock.On("Find", p.Account).Return(&account.Account{Balance:200}, nil).Once()
	th.accountsMock.On("Find", p.ToAccount).Return(&account.Account{}, fmt.Errorf("sql error")).Once()
	err = th.s.SendPayment(context.Background(), p)
	th.Require().Error(err)
	th.Require().Contains(err.Error(), "sql error")

	th.accountsMock.On("Find", p.Account).Return(&account.Account{Balance:200, Currency:"USD"}, nil).Once()
	th.accountsMock.On("Find", p.ToAccount).Return(&account.Account{Currency:"EUR"}, nil).Once()
	err = th.s.SendPayment(context.Background(), p)
	th.Require().Error(err)
	th.Require().IsType((*ErrInvalidArg)(nil), err)

	th.accountsMock.AssertExpectations(th.T())
}

func TestValidationServiceTestSuite(t *testing.T) {
	suite.Run(t, new(ValidationServiceTestSuite))
}
